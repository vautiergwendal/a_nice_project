#!/usr/bin/env/groovy

node{
    stage('Checkout'){
        checkout scm
        def pom = readMavenPom file: 'pom.xml'
        if(pom){
            echo "Building version ${pom.version}"
        }
    }
    stage('Build'){
        parallel 'build' : {
            withMaven(jdk: 'Java 17', maven: 'Maven 3.9.2') {
				sh "mvn compile"
                archiveArtifacts artifacts: '**/target/*.*'
            }
        }, 'JAI REUSSI' : {
            echo 'JAI REUSSI'
        }
    }
    stage('Test'){
        withMaven(jdk: 'Java 17', maven: 'Maven 3.9.2'){
            sh "mvn test"
        }
    }

    stage("build & SonarQube analysis") {
		withMaven(jdk: 'Java 17', maven: 'Maven 3.9.2') {
		  withSonarQubeEnv('SonarCloud') {
			sh 'mvn clean verify org.sonarsource.scanner.maven:sonar-maven-plugin:sonar -Dsonar.projectKey=vautiergwendal_a_nice_project'
		  }
		}
	}

}


